package fr.rougeciel.pocmusic;

import fr.rougeciel.pocmusic.commands.TestCommand;
import fr.rougeciel.pocmusic.events.JoinEvent;
import fr.rougeciel.pocmusic.objects.MusicThread;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class POCMusic extends JavaPlugin {

    private static POCMusic instance;

    private HashMap<Player, MusicThread> musicThreads;

    @Override
    public void onEnable() {
        instance = this;

        musicThreads = new HashMap<>();

        Bukkit.getPluginCommand("test").setExecutor(new TestCommand());
        Bukkit.getPluginCommand("stopmusic").setExecutor(new TestCommand());
        Bukkit.getPluginCommand("stopthread").setExecutor(new TestCommand());
        Bukkit.getPluginCommand("testskin").setExecutor(new TestCommand());
        getCommand("test").setTabCompleter(new SoundCompleter());

        Bukkit.getPluginManager().registerEvents(new JoinEvent(), this);
    }

    @Override
    public void onDisable() {
        for (MusicThread th : musicThreads.values()) {
            th.stopThread();
        }
        super.onDisable();
    }

    public HashMap<Player, MusicThread> getMusicThreads() {
        return musicThreads;
    }

    public static POCMusic getInstance() {
        return instance;
    }
}
