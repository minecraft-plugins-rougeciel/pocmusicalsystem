package fr.rougeciel.pocmusic;


public enum CustomSounds {
    JPP("jpp", 116000, false),
    GTA("gta", 34000, true),
    CREVE("creve", 24000, true);

    private final String key;
    private final int duration;
    private final boolean loop;

    CustomSounds(String var2, int duration, boolean loop) {
        this.key = var2;
        this.duration = duration;
        this.loop = loop;
    }

    public String key() {
        return this.key;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isLoop() {
        return loop;
    }
}
