package fr.rougeciel.pocmusic.commands;

import com.mojang.authlib.properties.Property;
import fr.rougeciel.pocmusic.CustomSounds;
import fr.rougeciel.pocmusic.POCMusic;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.skin.SkinnableEntity;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class TestCommand implements CommandExecutor {

    private String uuid = "5fbe2e3faeaa4a44bf3a56b6d710a57c";
    private String skin = "ewogICJ0aW1lc3RhbXAiIDogMTYxNDE2NDcxNTk3NiwKICAicHJvZmlsZUlkIiA6ICI1ZmJlMmUzZmFlYWE0YTQ0YmYzYTU2YjZkNzEwYTU3YyIsCiAgInByb2ZpbGVOYW1lIiA6ICJSb3VnZUNpZWwiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2RmMDRmODliMTIyZGNjOTU5NGYyODRiZjFlMmI1YTkyOTQ4MzI1YmYzOTU1NWEwNDlkOGE5NTM5NTE4OTE2OSIKICAgIH0KICB9Cn0=";
    private String signature = "QImWBtBLsTZBjo8TRTM904o6OI6AXmhoOesjkXlA2qW7U+GBiYRRSB8xXJyG1QJNHE0DotKO22fCxGyptcA3oQJ0LGRTDGa67YR+ooTgj/nFTEDNm22B7BNi2FOP8lXReFpF9eXR3VNE3+1btHY/MRAh2Ha8SeVCJo1DmIn8ah2kkmxsB5VmS7mx4GTweQlQNp5K7hCP6k9X3P0j/dnYCYURezePqU9cHBP7B7yGu6XkCfV+zw7cvOCTRzwzF8z5olNTmptvW1Vk1UVqA1x2ulJ9VVpC2OgnShZv+ibtU4MKfqBKdOWsJcQxgtgbql4BibgfZ3MEHrAJ+QnxnkIG+alg9bEgiqnnbHpEEv6k2XNDJxRE67dsjbDQNaTwQeBvlEDc8Guen+RIUCxir0CGIBxeY3sdPNSyHHtJbBuOJXPle3m822OalqOz7wFLOQe8g/RyLyzSOlAaUxACnuDKUv9hGgJuZMS6wuZMoV7SDE4KZ+YJZm0YTCONb/lKIVW74XK7rIOG6yE+wPJ0z1kb/DmL+E4Q9LKPcHvOUzBx/DrUAAWhOx3qumsRzLUPHYU0fSsZjBxadJs8UjWZlKX95MYoiMiEINGmdanuJzc9y1Ihq1ZSAcJjy++jzBD8reOGSieTtZBOA6sjlj+4PgDBUOyq4FpQKQqkRx0zWsieoOk=";

    private final POCMusic pocMusic = POCMusic.getInstance();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (command.getName().equalsIgnoreCase("test")) {
                if (args.length == 1) {
                    for(CustomSounds sound : CustomSounds.values()) {
                        if (sound.key().equalsIgnoreCase(args[0])) {
                            pocMusic.getMusicThreads().get(player).setSound(sound);
                        }
                    }
                }
            } else if (command.getName().equalsIgnoreCase("stopmusic")) {
                pocMusic.getMusicThreads().get(player).stopMusic();
            } else if (command.getName().equalsIgnoreCase("stopthread")) {
                pocMusic.getMusicThreads().get(player).stopThread();
            } else if (command.getName().equalsIgnoreCase("testskin")) {
                NPC npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "TEST");

                //GameProfile npcProfile = new GameProfile(UUID.randomUUID(), "Test");
                //npcProfile.getProperties().put("textures", new Property("textures", skin, signature));

                npc.data().set(NPC.PLAYER_SKIN_UUID_METADATA, uuid);
                npc.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_METADATA, skin);
                npc.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_SIGN_METADATA, signature);

//                EntityPlayer eP = new EntityPlayer(((CraftServer) Bukkit.getServer()).getServer(), ((CraftWorld) player.getWorld()).getHandle(), npcProfile, new PlayerInteractManager(((CraftWorld) player.getWorld()).getHandle()));
//                eP.teleportTo(((CraftWorld) player.getWorld()).getHandle(), new BlockPosition(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()));
//
//                Packet<PacketListenerPlayOut> infoPacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, eP);
//                Packet<PacketListenerPlayOut> entityPacket = new PacketPlayOutNamedEntitySpawn(eP);
//
//                PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
//
//                connection.sendPacket(infoPacket);
//                connection.sendPacket(entityPacket);

                npc.spawn(player.getLocation());

                SkinnableEntity entity = (SkinnableEntity) npc.getEntity();
                entity.getProfile().getProperties().put("textures", new Property("textures", skin, signature));

            }
        }
        return false;
    }
}
