package fr.rougeciel.pocmusic.events;

import fr.rougeciel.pocmusic.POCMusic;
import fr.rougeciel.pocmusic.objects.MusicThread;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinEvent implements Listener {

    private final POCMusic pocMusic = POCMusic.getInstance();

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!pocMusic.getMusicThreads().containsKey(player)) {
            pocMusic.getMusicThreads().put(player, new MusicThread(player));
            pocMusic.getMusicThreads().get(player).start();
        }
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (pocMusic.getMusicThreads().containsKey(player)) {
            pocMusic.getMusicThreads().get(player).stopThread();
            pocMusic.getMusicThreads().remove(player);
        }
    }
}
