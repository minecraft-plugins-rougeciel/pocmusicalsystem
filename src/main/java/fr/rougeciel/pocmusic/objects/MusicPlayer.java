package fr.rougeciel.pocmusic.objects;

import fr.rougeciel.pocmusic.CustomSounds;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class MusicPlayer {

    private String music;

    public void playMusic(Player player, CustomSounds sound) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    System.out.println(Thread.currentThread().getId() + " Played.");
                    ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutCustomSoundEffect(new MinecraftKey(sound.key()), SoundCategory.MUSIC, new Vec3D(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()), Float.MAX_VALUE, 1.0F));
                    try {
                        wait(93600);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.run();
    }

    public void stopMusic(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (CustomSounds so : CustomSounds.values()) {
                    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutStopSound(new MinecraftKey(so.key()), SoundCategory.MUSIC));
                }
                System.out.println(Thread.currentThread().getId() + " Stopped.");
            }
        }.run();
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
