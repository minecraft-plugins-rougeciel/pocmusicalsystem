package fr.rougeciel.pocmusic.objects;

import fr.rougeciel.pocmusic.CustomSounds;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class MusicThread extends Thread {
    private final Player player;
    private CustomSounds sound;
    private boolean exit;

    public MusicThread(Player player) {
        this.player = player;
        exit = false;
    }

    @Override
    public synchronized void run() {
        System.out.println("Thread " + this.getId() + " starting for player " + this.getPlayer().getDisplayName() + " ...");

        while (!exit) {
            if (sound == null) {
                try {
                    for (CustomSounds so : CustomSounds.values()) {
                        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutStopSound(new MinecraftKey(so.key()), SoundCategory.MUSIC));
                    }
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Start playing " + sound.name());
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutCustomSoundEffect(new MinecraftKey(sound.key()), SoundCategory.MUSIC, new Vec3D(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()), Float.MAX_VALUE, 1.0F));
            try {
                wait(sound.getDuration());
                if (sound != null && !sound.isLoop()) {
                    sound = null;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Stopping Thread " + this.getId() + "...");
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutStopSound(new MinecraftKey(sound.key()), SoundCategory.MUSIC));
    }

    public synchronized void stopThread() {
        this.exit = true;

        notify();
    }

    public synchronized void stopMusic() {
        this.sound = null;

        notify();
    }

    public synchronized void setSound(CustomSounds sound) {
        this.sound = sound;

        notify();
    }

    public Player getPlayer() {
        return player;
    }
}
