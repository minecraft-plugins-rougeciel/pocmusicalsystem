package fr.rougeciel.pocmusic;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SoundCompleter implements TabCompleter {

    private final List<String> SOUNDS;

    public SoundCompleter() {
        SOUNDS = new ArrayList<>();
        for (CustomSounds sound: CustomSounds.values()) {
            SOUNDS.add(sound.key());
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        final List<String> completions = new ArrayList<>();

        StringUtil.copyPartialMatches(args[0], SOUNDS, completions);
        Collections.sort(completions);

        return completions;
    }
}
